//testando o operador E - &&
namespace operador_E
{
let idade = 20;
let maiorIdade = idade > 18;
let possuiCarteiraDeMotorista = true;

let podeDigirir = maiorIdade && possuiCarteiraDeMotorista

console.log(podeDigirir); // true

}
